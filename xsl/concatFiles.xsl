<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:file="http://expath.org/ns/file"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:swrl="http://www.w3.org/2003/11/swrl#"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:p1="http://data.ub.uib.no/ontology"
    exclude-result-prefixes="xs file swrl p1 map saxon p1 rdf xsl owl"
    version="3.0">    
    
    <xsl:param name="extensions" select="'(\.owl$|\.xml$|\.rdf)'" as="xs:string"/>
    <xsl:param name="owl_name" select="()" as="xs:string?"/>    
    <xsl:param name="debug" as="xs:boolean" select="false()"/>
    <xsl:param name="owl_imports" as="xs:string?"/>        
        
    <xsl:variable name="base-dir" select="string-join(tokenize(base-uri(),'/')[position()=1 to last()-1],'/')"/>
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <xsl:if test="$debug">
            <xsl:message select="$owl_imports,'||',$owl_name,'||',$debug,'||',$extensions"></xsl:message>
        </xsl:if>
        <xsl:variable name="file-list" select="file:list($base-dir, false())" as="xs:string*"/>

        <xsl:if test="$debug">
            <xsl:message>
                <xsl:sequence select="$owl_name, $file-list"/>
            </xsl:message>
        </xsl:if>
        <!-- changing context to root node-->
        <xsl:for-each select="/rdf:RDF">
            <xsl:copy>
                <xsl:if test="string($owl_name) and not($owl_name castable as xs:anyURI)">
                    <xsl:message terminate="yes" expand-text="1">{$owl_name} illegal
                        uri</xsl:message>
                </xsl:if>

                <xsl:if test="exists($owl_imports) and not(string($owl_name))">
                    <xsl:message terminate="yes"> owl:imports requires owl:name to be set.
                    </xsl:message>
                </xsl:if>

                <!-- add xml:base to copy-exception if owl_name is set-->
                <xsl:copy-of
                    select="
                        @* except @xml:base"/>
                <!-- override xml:base if exists, keep if no owl_name is set-->
                <xsl:if test="@xml:base">
                    <xsl:attribute name="xml:base" select="($owl_name,@xml:base)[1]"/>
                </xsl:if>
                <xsl:variable name="namespace-map" as="map(xs:string,xs:string)">
                    <xsl:call-template name="getRDFNameSpaces">
                        <xsl:with-param name="file-list" select="$file-list"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:for-each select="map:keys($namespace-map)">
                    <xsl:namespace name="{.}" select="map:get($namespace-map, .)"/>
                </xsl:for-each>

                <xsl:call-template name="copyFiles">
                    <xsl:with-param name="file-list" select="$file-list"/>
                </xsl:call-template>

                <xsl:call-template name="writeOwlImport"/>
            </xsl:copy>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="rdf:RDF|/" mode="copy" priority="1.0">    
        <xsl:apply-templates mode="copy"/>
    </xsl:template>
    
    <!-- remove old ontology information if $owl_name is set-->
    <xsl:template match="rdf:Description[rdf:type/@rdf:resource='http://www.w3.org/2002/07/owl#Ontology' and (string($owl_name))]" priority="1" mode="copy"/>

    <xsl:template match="*" mode="copy" priority="0.9">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="copy"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template name="getRDFNameSpaces">
        <xsl:param name="file-list" as="xs:string+"/>
        <xsl:param name="initial-namespaces" as="map(xs:string,xs:string)?"/>
        <xsl:iterate select="$file-list">
            <xsl:param name="namespaces" as="map(xs:string,xs:string)?">
               <xsl:map></xsl:map>
            </xsl:param>
            <xsl:on-completion>
                <xsl:sequence select="$namespaces"/>
            </xsl:on-completion>       
            <xsl:choose>
                <xsl:when test="matches(., $extensions, 'i')">
                    <xsl:variable name="map" as="map(xs:string,xs:string)?">
                        <xsl:variable name="doc" select="document(concat($base-dir, '/', .))"/>
                        <xsl:map>
                            <xsl:for-each select="$doc/rdf:RDF/namespace::*">
                                <xsl:variable name="ns-uri" as="xs:string" select="string(.)"/>
                                <xsl:variable name="prefix" as="xs:string" select="local-name(.)"/>
                                <xsl:if
                                    test="
                                        exists($namespaces) and (map:contains($namespaces, $prefix)
                                        and map:get($namespaces, $prefix) != $ns-uri)">
                                    <xsl:message terminate="yes" expand-text="1"> mismatch namespace
                                        prefix: {$prefix}, uri1: {map:get($namespaces,$prefix)}
                                        uri2: {$ns-uri} </xsl:message>
                                </xsl:if>
                                <xsl:if
                                    test="not(exists($namespaces)) or not(map:contains($namespaces, $prefix))">
                                    <xsl:if test="$debug">
                                        <xsl:message>
                                            <xsl:value-of select="., $ns-uri"/>
                                        </xsl:message>
                                    </xsl:if>
                                    <xsl:map-entry select="$ns-uri" key="$prefix"/>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:map>
                        <xsl:for-each select="saxon:discard-document($doc)">
                          <xsl:if test="$debug">
                              <xsl:message select="concat($doc, ' discarded')"/>
                          </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:next-iteration>
                        <xsl:with-param name="namespaces" select="map:merge(($namespaces, $map))"/>
                    </xsl:next-iteration>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:next-iteration/>
                </xsl:otherwise>
            </xsl:choose>                        
        </xsl:iterate>
    </xsl:template>
    
    <xsl:template name="copyFiles">
        <xsl:param name="file-list" as="xs:string+"/>
        <xsl:for-each select="$file-list[matches(.,$extensions,'i')]">
            <xsl:message><xsl:value-of select="."/></xsl:message>
            <xsl:apply-templates select="saxon:discard-document(document(concat($base-dir,'/',.)))" mode="copy"/>            
        </xsl:for-each>
    </xsl:template>
   
    <xsl:template name="writeOwlImport">
        <xsl:if test="string($owl_name)">
        <rdf:Description rdf:about="{$owl_name}">  
            <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#Ontology"/>
            <xsl:for-each select="tokenize($owl_imports,' ')">
                <xsl:if test="not(. castable as xs:anyURI)">
                <xsl:message terminate="yes">
                    <xsl:text expand-text="1">illegal import uri {.}</xsl:text>
                </xsl:message>
                </xsl:if>                
                <owl:imports rdf:resource="{.}"/> 
            </xsl:for-each>
        </rdf:Description>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>